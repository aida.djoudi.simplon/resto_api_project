<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\BookingRepository;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Symfony\Component\Serializer\Annotation\Groups;


/**
 * @ApiResource(
 *  normalizationContext={"groups"={"read"}},
 *  attributes={
 *      "order"={"registrationDate":"DESC"}
 *  },
 *  paginationItemsPerPage=5,
 *  itemOperations={
 *       "get" = { "security" = "is_granted('show', object)" ,"security_message"="Sorry,you are not the adding of this booking."},
 *       "delete" = { "security" = "is_granted('delete', object)","security_message"="Sorry,you are not the adding of this booking so you can not deleted" },
 *       "put" = { "security" = "is_granted('edit', object)",
 *                  "security_message"="Sorry,you are not the adding of this booking so you can not editing",
 *                   "denormalization_context"={"groups"={"update:booking"}}
 *        }
 *  }
 * )
 * @ApiFilter(SearchFilter::class,properties={"user":"exact"})
 * @ORM\Entity(repositoryClass=BookingRepository::class)
 */
class Booking
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"read"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="bookings")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"read"})
     * 
     */
    private $user;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank(message ="numéro invitées est obligatoire")
     * @Assert\Range(
     *      max = 5,
     *      notInRangeMessage = "le nombre maximum  est {{ max }}invités"
     * )
     * @Groups({"create:booking","update:booking","read"})
     * 
     */
    private $numberGuests;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"create:booking","update:booking","read"})
     */
    private $comment;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank(message ="date et heure est obligatoire")
     * @Groups({"create:booking","update:booking","read"})
     */
    private $bookingDate;
    /**
     * @ORM\Column(type="datetime")
     */
    private $registrationDate;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getNumberGuests(): ?int
    {
        return $this->numberGuests;
    }

    public function setNumberGuests(int $numberGuests): self
    {
        $this->numberGuests = $numberGuests;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getBookingDate(): ?\DateTimeInterface
    {
        return $this->bookingDate;
    }

    public function setBookingDate(\DateTimeInterface $bookingDate): self
    {
        $this->bookingDate = $bookingDate;

        return $this;
    }
    public function getRegistrationDate(): ?\DateTimeInterface
    {
        return $this->registrationDate;
    }

    public function setRegistrationDate(\DateTimeInterface $registrationDate): self
    {
        $this->registrationDate = $registrationDate;

        return $this;
    }

}
