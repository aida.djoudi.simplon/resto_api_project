<?php

namespace App\Security\Voter;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class BookingVoter extends Voter 
{
    protected function supports($attribute, $subject)
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, ['show', 'edit', 'delete'])
            && $subject instanceof \App\Entity\Booking;
    }
    protected function voteOnAttribute($attribute, $booking, TokenInterface $token)
    {
        $user = $token->getUser();
        //dd($booking->getUser());
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }
        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case 'show':
                return $booking->getUser()==$user;
                break;
            case 'edit':
                return $booking->getUser()==$user;
                break;
            case 'delete':
                return $booking->getUser()==$user;    
                break;
        }
        return false;
    }
}
