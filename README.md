

💻 How to use

   1.git clone https://gitlab.com/aida.djoudi.simplon/resto_api_project.git
   

   in .env.local, fill: DATABASE_URL=mysql://db_user:db_password@127.0.0.1:3306/db_name?serverVersion=5.7

    if necesary replace db_user and db_password and db_name 

   run these commands:

   composer install

   php bin/console doctrine:database:create
   php bin/console make:migration
   php bin/console doctrine:migration:migrate



   ⚡️ Requirements

A simple database with MySQL



Good luck, developer! 🚀
    
